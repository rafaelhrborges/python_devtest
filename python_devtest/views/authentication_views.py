from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from python_devtest.serializers import UserSerializer
from django.contrib.auth.models import User

class AuthenticateView(APIView):
    def post(self, request):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            try:
                user = User.objects.get(username=serializer.data['username'])
                if user.check_password(serializer.data['password']):
                    return Response({"success": True}, \
                        status=status.HTTP_200_OK)
                else:
                    return Response({ "password": "Senha inválida."}, \
                        status=status.HTTP_403_FORBIDDEN)
            except Exception as error:
                print(error)
                return Response({ "username": "Usuário inválido"}, \
                    status=status.HTTP_403_FORBIDDEN)
