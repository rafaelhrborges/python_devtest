from rest_framework.serializers import CharField, ModelSerializer, Serializer
from django.contrib.auth.models import User

class UserSerializer(Serializer):
    username = CharField(max_length=40)
    password = CharField(max_length=40)

class UserCreateSerializer(ModelSerializer):
    username = CharField(max_length=40)
    password = CharField(max_length=40, write_only=True)

    class Meta:
        model = User
        fields = ('username', 'password')

    def create(self, validated_data):
        user = super(UserCreateSerializer, self).create(validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user